-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2017 at 12:09 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `metajua`
--

-- --------------------------------------------------------

--
-- Table structure for table `form_components`
--

CREATE TABLE `form_components` (
  `id` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `label` varchar(100) NOT NULL,
  `instructions` varchar(150) NOT NULL,
  `order` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_components`
--

INSERT INTO `form_components` (`id`, `type`, `label`, `instructions`, `order`) VALUES
(1, 1, 'Name', 'Enter your name', 1),
(2, 5, 'Gender', 'Select your gender', 2),
(4, 2, 'Crops grown', 'Tick the crops you grow', 4),
(5, 4, 'Comments', 'Enter any additional comments on the farmer', 5);

-- --------------------------------------------------------

--
-- Table structure for table `form_components_meta`
--

CREATE TABLE `form_components_meta` (
  `id` int(11) NOT NULL,
  `form_component` int(5) NOT NULL,
  `key` varchar(250) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_components_meta`
--

INSERT INTO `form_components_meta` (`id`, `form_component`, `key`, `value`) VALUES
(1, 2, 'options', 'male,female'),
(2, 2, 'values', 'Male,Female');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `form_components`
--
ALTER TABLE `form_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_components_meta`
--
ALTER TABLE `form_components_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `form_components`
--
ALTER TABLE `form_components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `form_components_meta`
--
ALTER TABLE `form_components_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
